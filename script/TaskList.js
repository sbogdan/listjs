/**
 * Represents a TaskList class
 * @constructor
 */
function TaskList() {

    this.data = [];

    /**
     * Returns id of the last task
     * @param data
     * @returns {*}
     */
    var getMaxId = function (data) {
        if (data.length) {
            return data.reduce(function (max, current) {
                return current.id > max ? current.id : max;
            }, 0)
        } else
            return -1;
    };

    /**
     * Adds task to tasks list
     * @param {string} description defines description of current task
     * @returns {Task}
     */
    this.addTask = function (description) {
        var item = new Task(getMaxId(this.data) + 1, description);
        this.data.push(item);
        return item;
    };

    /**
     * Sets new description to the current task
     * @param id defines id of current task
     * @param newDescription
     */
    this.editTask = function (id, newDescription){
        this.getTaskById(id).description = newDescription;
    };


    /**
     * Removes current task
     * @param id defines id of current task
     */
    this.deleteTask = function (id) {
        this.data.forEach(function (item, i, array) {
            if (item.id == id) {
                array.splice(i, 1);
            }
        })
    };

    /**
     * Returns array containing all tasks
     * @returns {Array}
     */
    this.getData = function () {
        return this.data
    };

    /**
     * Returns task by id
     * @param id defines id of current task
     * @returns {Task}
     */
    this.getTaskById = function (id) {
        var task = null;
        this.data.forEach(function (item) {

            if (item.id == id) {
                task = item;
            }
        });
        return task;
    }


}
