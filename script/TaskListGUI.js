/**
 * Represents a TaskList class
 * @constructor
 */
function TaskListGUI() {
    /**
     * Sets default HTML tags properties
     */
    var defaults = {
        inputArea: "#input-area",
        newTask: "#new-task",
        addButton: "#add-button",
        showButton: "#show-button",
        hideButton: "hide-button",

        taskList: "#task-list",
        taskBody: "task-body",
        taskId: "task-",
        checkboxWrapper: "checkboxWrapper",
        taskDescription: "task-description",
        editTask: "edit-task",
        deleteTask: "delete-task",
        completed: "completed",
        hidden: "hidden",

        editField: "edit-field",
        editOk: "edit-ok",
        editCancel: "edit-cancel",
        editArea: "edit-area"
    };

    var tList = new TaskList();

    /**
     * Generates task view
     * @param {Task} task
     */
    var generateTask = function (task) {
        var parent = $(defaults.taskList);
        var wrapper = $("<div />", {
            "class": defaults.taskBody,
            "id": defaults.taskId + task.id
        }).appendTo(parent);

        var checkboxWrapper = $("<div />", {
            "class": defaults.checkboxWrapper
        }).appendTo(wrapper);

        $("<input />", {
            "type": "checkbox",
            "checked": task.done
        }).appendTo(checkboxWrapper);

        $("<div />", {
            "class": task.done
                ? defaults.taskDescription + " " + defaults.completed
                : defaults.taskDescription,
            "text": task.description
        }).appendTo(wrapper);

        $("<button />", {
            "class": defaults.editTask,
            "text": "edit"
        }).appendTo(wrapper);
        $("<button />", {
            "class": defaults.deleteTask,
            "text": "delete"
        }).appendTo(wrapper);


    };

    /**
     * Generates list of tasks view
     */
    var generateTasksList = function () {
        var tasksArray = tList.getData();
        tasksArray.forEach(function (item) {
            generateTask(item);
        });
    };

    /**
     * Makes description text view outlined
     */
    var outlineDescription = function () {
        var checkbox = event.target;

        if (checkbox.type !== 'checkbox') return;

        var taskContainer = $(checkbox).closest("." + defaults.taskBody);
        var taskDescription = taskContainer.children("." + defaults.taskDescription);
        var taskId = taskContainer.prop("id").substr(defaults.taskId.length);


        if (checkbox.checked){
            taskDescription.addClass(defaults.completed);
            tList.getTaskById(taskId).setDone(true);

        } else {
            taskDescription.removeClass(defaults.completed);
            tList.getTaskById(taskId).setDone(false);

        }

    };

    /**
     * Hides or shows checked tasks
     */
    var hideChecked = function () {
        var button = $(this);

        if (button.attr("id") === defaults.hideButton) {
            button.attr("id", defaults.showButton);
            button.text('Show checked');
            setHidden(true);
        } else {
            button.attr("id", defaults.hideButton);
            button.text('Hide checked');
            setHidden(false);
        }
    };

    /**
     * Sets tasks hidden or shown
     * @param {boolean} hide defines if task should be hidden
     */
    var setHidden = function (hide) {
        var elements = $("." + defaults.taskBody);
        elements.each(function (i, value) {
            if (!hide && $(value).hasClass(defaults.hidden)) {
                $(value).removeClass(defaults.hidden);

            }
            if (hide && $(value).children("." + defaults.taskDescription).hasClass(defaults.completed)) {
                $(value).addClass(defaults.hidden);

            }
        });
    };

    /**
     * Adds new task
     */
    var addNewTask = function(){
        var taskDescription = $(defaults.newTask).val();
        var newTask = tList.addTask(taskDescription);
        generateTask(newTask);
    };

    /**
     * Returns id of current task
     * @param taskContainer defines container of visual elements of current task
     * @returns {number}
     */
    var getCurrentId = function (taskContainer) {
        return + taskContainer.prop("id").substr(defaults.taskId.length);
    };

    /**
     * Applies changes to the task description
     * @param taskContainer defines container of visual elements of current task
     */
    var applyChanges = function (taskContainer) {
        var taskId = getCurrentId(taskContainer);
        var task = tList.getTaskById(taskId);
        var editArea = taskContainer.children("#" + defaults.editArea);
        var newDescription = $(editArea).children("#" + defaults.editField).val();
        var taskDescription = taskContainer.children("." + defaults.taskDescription);

        task.setDescription(newDescription);
        taskDescription.html(newDescription);
        clearChangeDialogs();

    };

    /**
     * Handles event of clicking on a tasks list and its children
     */
    var taskListClick = function(){
        var eventTarget = $(event.target);
        var taskContainer = eventTarget.closest("." + defaults.taskBody);
        var currentClass = eventTarget.prop('class');
        if(currentClass === defaults.deleteTask){
            deleteTask(taskContainer);
        }
        if(currentClass == defaults.editTask){
            initChangeDialog(taskContainer);
        }
        if(currentClass == defaults.editCancel){
            clearChangeDialogs();
        }
        if(currentClass == defaults.editOk){
            applyChanges(taskContainer);
        }
    };

    /**
     * Removes current task
     * @param taskContainer defines container of visual elements of current task
     */
    var deleteTask = function(taskContainer){
        var taskId = getCurrentId(taskContainer);
        taskContainer.remove();
        tList.deleteTask(taskId);
    };

    /**
     * Removes editing dialogs
     */
    var clearChangeDialogs = function (){
        var elements = $("#" + defaults.editArea);
        elements.each(function(i, value){
            value.remove();
        })
    };

    /**
     * Generates view of editing dialog
     * @param taskContainer defines container of visual elements of current task
     */
    var generateEditDialog = function (taskContainer) {
        var currentDescription = taskContainer.children("." + defaults.taskDescription).text();
        var wrapper = $("<div />", {
            "id": defaults.editArea
        }).appendTo(taskContainer);
        $("<input />", {
            "id": defaults.editField,
            "type": "text",
            "value": currentDescription
        }).appendTo(wrapper);
        $("<button />", {
            "class": defaults.editOk,
            "text": "apply"
        }).appendTo(wrapper);
        $("<button />", {
            "class": defaults.editCancel,
            "text": "cancel"
        }).appendTo(wrapper);
    };

    /**
     * Adds editing dialog to the current task container
     * @param taskContainer defines container of visual elements of current task
     */
    var initChangeDialog = function (taskContainer){
        clearChangeDialogs();
        generateEditDialog(taskContainer);
    };


    /**
     * Adds tasks list to the page
     */
    this.init = function () {
        generateTasksList();
    };

    /**
     * Set events to the tasks list's visual elements
     */
    (function (){
        $(defaults.addButton).click(addNewTask);
        $(defaults.taskList).change(outlineDescription);
        $(defaults.taskList).click(taskListClick);
        $("#" + defaults.hideButton).click(hideChecked);
    })();


}



