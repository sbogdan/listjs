/**
 * Represents a Task class
 * @param id defines task id
 * @param description defines description of the task
 * @constructor
 */
function Task(id, description){
    this.id = id;
    this.description = description;
    this.done = false;

    /**
     * Sets task state
     * @param value defines if task is being done
     */
    this.setDone = function(value){
        this.done = value;
    };
    /**
     * Sets description of the task
     * @param description
     */
    this.setDescription = function (description){
       this.description = description
    };
}